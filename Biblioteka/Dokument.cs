﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public abstract class Dokument
    {
        public string ISBN { get; set; }
        public string tytul { get; set; }
        public string  wydawnictwo { get; set; }
        public int liczbaStron { get; set; }

        public Dokument()
        {

        }

        public Dokument(string ISBN, string tytul, string wydawnictwo, int liczbaStron)
        {
            this.ISBN = ISBN;
            this.tytul = tytul;
            this.wydawnictwo = wydawnictwo;
            this.liczbaStron = liczbaStron;
        }

        public override string ToString()
        {
            return new StringBuilder($"ISBN: {ISBN}, tytul: {tytul}, wydwanictwo: {wydawnictwo}, Liczba stron: {liczbaStron}").ToString();
        }
        public override bool Equals(Object obj)
        {
            Dokument d = (Dokument)obj;
            if(ISBN == d.ISBN && tytul == d.tytul && wydawnictwo== d.wydawnictwo && liczbaStron== d.liczbaStron)
            {
                return true;
            }else
            {
                return false;
            }
        }
        public override int GetHashCode()
        {
            return ISBN.GetHashCode();
        }
        
    }
}
