﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Ksiazka : Dokument
    {
        public string Autor { get; set; }
        public int rokWydania { get; set; }
        public Ksiazka(string ISBN, string tytul, string wydawnictwo, int liczbaStron, string Autor, int rokWydania) :base(ISBN,tytul, wydawnictwo,liczbaStron)
        {
            this.Autor = Autor;
            this.rokWydania = rokWydania;
        }
        public override bool Equals(object obj)
        {
            Ksiazka k =(Ksiazka) obj;
            if (base.Equals(obj) && Autor==k.Autor && rokWydania==k.rokWydania)
            {
                return true;
            }else
            {
                return false;
            }
        }
        public override string ToString()
        {
            return new StringBuilder(base.ToString() + $" Autor: {Autor} Rok wydania: {rokWydania}").ToString();
        }
        public override int GetHashCode()
        {
            return base.GetHashCode()+Autor.GetHashCode();
        }
    }
}
