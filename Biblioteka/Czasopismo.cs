﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Czasopismo : Dokument
    {
        public int numer { get; set; }
        public Czestotliwosc czestotliwosc { get; set; }
        public Czasopismo(string ISBN, string tytul, string wydawnictwo, int liczbaStron, int numer, Czestotliwosc czestotliwosc ) : base(ISBN,tytul,wydawnictwo,liczbaStron)
        {
            this.numer=numer;
            this.czestotliwosc = czestotliwosc;
        }
        public override string ToString()
        {
            return  new StringBuilder(base.ToString()+ $" Numer: {numer}, Czestotliwosc: {czestotliwosc} ").ToString();
        }
        public override bool Equals(object obj)
        {
            return ToString() == obj.ToString();
        }
        public override int GetHashCode()
        {
            return numer.GetHashCode();
        }
    }
    public enum Czestotliwosc
    {
        Dziennik,
        Tygodnik,
        Miesiecznik
    }
}
