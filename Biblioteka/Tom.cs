﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Biblioteka
{
    public class Tom : Ksiazka
    {
        public int numerTomu { get; set; }
        public static int iloscTomow { get; set; }
       
        public Tom(string ISBN, string tytul, string wydawnictwo, int liczbaStron, string Autor, int rokWydania,int numerTomu, int iloscTomow ) : base(ISBN, tytul,wydawnictwo,liczbaStron,Autor,rokWydania)
        {
            this.numerTomu = numerTomu;
            Tom.iloscTomow = iloscTomow;
        }
        //public Tom(Ksiazka k)
        //{

        //}
        public override string ToString()
        {
            return new StringBuilder(base.ToString()+ $" Numer tomu: {numerTomu} Ilosc tomow: {iloscTomow}").ToString();

        }
        public override bool Equals(object obj)
        {
            return this.ToString() == obj.ToString();
        }
    }
}
