﻿using Biblioteka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB3
{
    class Program
    {
        static void Main(string[] args)
        {
            //  Managment.dodajDoListy(new Ksiazka(122, "dzici", "wydawnictwo", 200, "gall", 2001));
            //  Managment.dodajDoListy(new Tom(112, "loool", "asdasdasd",200,"gall",1994,200,10));
            Managment.dodajDoListy(new Ksiazka("978 - 83 - 283 - 2424 - 4", "C# 6.0 in a Nutshell: The Definitive Reference, 6th Edition", "Helion", 1000, "Robert Górczyński", 2015));
            Managment.dodajDoListy(new Ksiazka("978-0596805524", "JavaScript: The Definitive Guide: Activate Your Web Pages", "O'Reilly Media", 1096, "David Flanagan", 2011));
            Managment.dodajDoListy(new Ksiazka("978-1491904190", "You Don't Know JS: Types & Grammar", "O'Reilly Media", 198, "Kyle Simpson", 2015));
            Managment.dodajDoListy(new Czasopismo("111111111", "BRAVO GIRL", "BRAVO WYDAWNICTWO", 30, 1, Czestotliwosc.Tygodnik));
            Managment.dodajDoListy(new Czasopismo("222222222", "Gazeta wyborcza", "Wyborcze wydawnictwo", 30, 100, Czestotliwosc.Dziennik));
            Managment.dodajDoListy(new Czasopismo("3333333333", "Newseweek", "Newsweek wydawnictwo", 40, 30, Czestotliwosc.Miesiecznik));
            Managment.dodajDoListy(new Tom("1112222333", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 2000,1,3));
            Managment.dodajDoListy(new Tom("1112222334", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 2002, 2, 3));
            Managment.dodajDoListy(new Tom("1112222335", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 2004, 3, 3));
            Console.WriteLine("Wyswietlam wszystko: ");
            Managment.wyswietlWszystko();
            Console.WriteLine("Usuwanie z listy: ");
            Managment.usunDokument(Managment.getDokument("978-0596805524"));
            Managment.usunDokument("978 - 83 - 283 - 2424 - 4");
            Console.WriteLine("wYSWIETLAM LISTE: ");
            Managment.wyswietlWszystko();
            List<Dokument> lista=Managment.getListaPoFrazie("Wladca");
            Console.WriteLine("FRAZY WYSWIETLAM");
      
            lista.ForEach(k =>
            {
                Console.WriteLine(k.ToString());
            });
            Console.WriteLine("Wyswietlam tylko zawerzona czestotliwosc");
            List<Dokument> lista2=Managment.getCzasopismapoCzestotliwosci(Czestotliwosc.Dziennik);
            lista2.ForEach(k =>
            {
                Console.WriteLine(k.ToString());
            });
            Console.WriteLine("Wyswietlam liste tomow o podanym tytule");
            List<Tom> lista3=Managment.getListaTomow("Wladca Pierscieni");
            lista3.ForEach(k =>
            {
                Console.WriteLine(k.ToString());
            });

            Console.WriteLine("Watki: ");
            try
            {
                // Managment.dodajDoListy(new Czasopismo("222222222", "Gazeta wyborcza", "Wyborcze wydawnictwo", 30, 100, Czestotliwosc.Dziennik));
                //Managment.dodajDoListy(new Tom("1112222333335", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 2004, 3, 3));
                //  Managment.dodajDoListy(new Tom("1112222333335", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 2004, 5, 3));
            //    Managment.dodajDoListy(new Tom("1112222333335", "Wladca Pierscieni", "Pierscieni wydwanictwo", 1000, "Tolkien", 1439, 2, 3));

            }
            catch (BylTakiIsbn e)
            {
                WyswietlInfo.info(e);
            }
            catch(Exception e)
            {
                WyswietlInfo.info(e);
            }
           


            Console.ReadKey();
        }
    }
}
