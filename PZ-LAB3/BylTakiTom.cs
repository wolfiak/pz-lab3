﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB3
{

    [Serializable]
    public class BylTakiTom : Exception
    {
        public BylTakiTom() { }
        public override string Message
        {
            get
            {
                return "Byl juz taki TOM KOLEGO! ";
            }
        }
    }
}
