﻿using Biblioteka;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PZ_LAB3
{
    static class Managment
    {
        private static List<Dokument> lista { get; set; } = new List<Dokument>();

        public static void dodajDoListy(Dokument d)
        {
           if(lista.Where(o=> d.ISBN == o.ISBN).Count() > 0)
            {
                throw new BylTakiIsbn();
            }
           if(d is Tom)
            {
                Tom t=(Tom) d;
                Ksiazka k =(Ksiazka) d;
                lista.ForEach(o =>
                {
                    if (o is Tom)
                    {
                        Tom dopo = (Tom)(Ksiazka)o;
                        if (t.numerTomu == dopo.numerTomu)
                        {
                            throw new BylTakiTom();
                        }
                        if (t.numerTomu > Tom.iloscTomow)
                        {
                            throw new Exception("Numer tomu wieszy niz ilosc tomow");
                        }
                        if (k.rokWydania < 1440)
                        {
                            throw new Exception("Nie wynaleziono jescze druku");
                        }
                    }
                });
            }
            lista.Add(d);
        }

        public static bool usunDokument(Dokument d)
        {
            return lista.Remove(d);
        }

        public static bool usunDokument(string ISBN)
        {
            int n=lista.RemoveAll(o => o.ISBN == ISBN );
            if (n > 0)
            {
                return true;
            }
            else
            {
                return false;
            }
            
        }
        public static Dokument getDokument(string ISBN)
        {
            return lista.Single(o => o.ISBN == ISBN);
        }
        public static List<Dokument> getListaPoFrazie(string fraza)
        {
            List<Dokument> zwracany = new List<Dokument>();
            StringBuilder sb = new StringBuilder(fraza);
            lista.ForEach(o =>
            {
                if (o.tytul.Contains(sb.ToString()))
                {
                    zwracany.Add(o);
                }
            });
            return zwracany;
        }
        public static List<Dokument> getCzasopismapoCzestotliwosci(Czestotliwosc c)
        {
            List<Dokument> zwracany = new List<Dokument>();
            lista.ForEach(o =>
            {
                if (o is Czasopismo)
                {
                    Czasopismo co = (Czasopismo)o;
                    if (co.czestotliwosc == c)
                    {
                        zwracany.Add(co);
                    }
                }
            });
            return zwracany;

        }
        public static List<Tom> getListaTomow(string tytul)
        {
            List<Tom> zwracany = new List<Tom>();
            lista.ForEach(o =>
            {
                if(o.tytul == tytul)
                {
                    zwracany.Add((Tom) o);
                }
            });
            return zwracany;
        }
        public static void wyswietlWszystko()
        {
            lista.ForEach(k =>
            {
                Console.WriteLine(k.ToString());
            });
        }
    }
}
